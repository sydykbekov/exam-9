import React from 'react';
import axios from 'axios';

import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';

import reducer from './src/store/reducer';
import MyApp from "./src/containers/MyApp/MyApp";
import thunk from 'redux-thunk';

const store = createStore(reducer, applyMiddleware(thunk));

axios.defaults.baseURL = 'https://exam-9-1fe90.firebaseio.com/';

const App = props => (
    <Provider store={store}>
        <MyApp />
    </Provider>
);

export default App;