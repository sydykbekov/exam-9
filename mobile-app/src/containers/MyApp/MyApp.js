import React, {Component} from 'react';
import {connect} from "react-redux";
import {View, StyleSheet, Text, FlatList, Image, TouchableOpacity, Modal, Button} from "react-native";
import Preloader from '../../assets/Preloader.gif';
import {getContacts} from "../../store/actions";

class MyApp extends Component {
    state = {
        pickedContact: {},
        modalVisible: false
    };

    componentDidMount() {
        this.props.getContacts();
    }

    infoToModal = (contact) => {
        this.setState({pickedContact: contact, modalVisible: true});
    };

    render() {
        if (this.props.loading) {
            return <View style={styles.preloader}><Image source={Preloader} /></View>;
        }
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.contacts}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) =>
                        <TouchableOpacity onPress={() => this.infoToModal(item)}>
                            <View style={styles.listContainer}>
                                <Image style={styles.image} source={{uri: item.photo}} />
                                <Text style={styles.text}>{item.name}</Text>
                            </View>
                        </TouchableOpacity>
                    }
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <View style={styles.contact}>
                        <Image style={styles.photo} source={{uri: this.state.pickedContact.photo}} />
                        <Text style={styles.info}>{this.state.pickedContact.name}</Text>
                        <Text style={styles.info}>Phone: {this.state.pickedContact.phone}</Text>
                        <Text style={styles.info}>Email: {this.state.pickedContact.email}</Text>
                    </View>
                    <Button
                        onPress={() => this.setState({modalVisible: false})}
                        title="Back to contacts"
                        color="black"
                    />
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        alignItems: 'center'
    },
    listContainer: {
        width: '100%',
        backgroundColor: 'black',
        padding: 10,
        marginTop: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        width: '30%',
        height: 80,
        borderRadius: 5
    },
    text: {
        width: '70%',
        padding: 10,
        color: 'grey',
        fontSize: 20
    },
    preloader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    photo: {
        width: 200,
        height: 200
    },
    info: {
        fontSize: 20
    },
    contact: {
        flex: 1,
        alignItems: 'center',
        marginTop: 50
    }
});

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getContacts: () => dispatch(getContacts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyApp);

