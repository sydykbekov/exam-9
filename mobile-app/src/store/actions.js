import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';

export const startRequest = () => {
    return {type: START_REQUEST}
};

export const successRequest = (response) => {
    return {type: SUCCESS_REQUEST, value: response};
};

export const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const getContacts = () => {
    return dispatch => {
        dispatch(startRequest());
        axios.get('contacts.json').then(response => {
            const contacts = [];
            for (let key in response.data) {
                contacts.push({...response.data[key], id: key});
            }
            dispatch(successRequest(contacts));
        }, error => {
            dispatch(errorRequest());
        });
    }
};