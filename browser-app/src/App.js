import React, {Component, Fragment} from 'react';
import WebApp from "./containers/WebApp/WebApp";
import {Route, Switch} from "react-router-dom";
import Editor from "./containers/Editor/Editor";
import AddNewContact from "./containers/AddNewContact/AddNewContact";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Switch>
                    <Route path="/" exact component={WebApp} />
                    <Route path="/add" component={AddNewContact} />
                    <Route path="/edit" component={Editor} />
                    <Route render={() => <h1>404 NOT FOUND</h1>} />
                </Switch>
            </Fragment>
        );
    }
}

export default App;
