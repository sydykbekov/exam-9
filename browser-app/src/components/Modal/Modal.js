import React from 'react';
import './Modal.css';

const Modal = props => {
    return (
        <div className="modal">
            <img src={props.contact.photo} alt=""/>
            <h3>{props.contact.name}</h3>
            <p>Phone: {props.contact.phone}</p>
            <p>Email: {props.contact.email}</p>
            <button onClick={() => props.toEdit(props.contact)} style={{marginRight: '50px'}}>Edit</button>
            <button onClick={() => props.remove(props.contact.id)}>Delete</button>
        </div>
    )
};

export default Modal;