import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <div className="form-container">
            <label htmlFor=""><span>Name: </span><input onChange={props.changeName} value={props.contacts.name} type="text"/></label><br/>
            <label htmlFor=""><span>Phone: </span><input onChange={props.changePhone} value={props.contacts.phone} type="text"/></label><br/>
            <label htmlFor=""><span>Email: </span><input onChange={props.changeEmail} value={props.contacts.email} type="text"/></label><br/>
            <label htmlFor=""><span>Photo: </span><input onChange={props.changePhoto} value={props.contacts.photo} type="text"/></label><br/>
            <span>Photo preview: </span><img src={props.contacts.photo} alt=""/><br/>
            <button onClick={props.add} style={{marginRight: '50px'}}>Save</button><button onClick={props.back}>Back to contacts</button>
        </div>
    )
};

export default Form;