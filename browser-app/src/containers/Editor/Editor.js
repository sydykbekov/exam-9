import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import {connect} from "react-redux";
import {saveChanges} from "../../store/actions";

class Editor extends Component {
    state = {
        contacts: {
            name: '',
            phone: '',
            email: '',
            photo: ''
        }
    };

    componentDidMount() {
        const contacts = JSON.parse(localStorage.getItem('contacts'));
        this.setState({contacts});
    }

    changeName = (event) => {
        const contacts = {...this.state.contacts};
        contacts.name = event.target.value;
        this.setState({contacts});
    };

    changePhone = (event) => {
        const contacts = {...this.state.contacts};
        contacts.phone = event.target.value;
        this.setState({contacts});
    };

    changeEmail = (event) => {
        const contacts = {...this.state.contacts};
        contacts.email = event.target.value;
        this.setState({contacts});
    };

    changePhoto = (event) => {
        const contacts = {...this.state.contacts};
        contacts.photo = event.target.value;
        this.setState({contacts});
    };

    back = () => {
        this.props.history.push({
            pathname: '/'
        })
    };

    saveContact = () => {
        this.props.save(this.state.contacts, this.props.history);
    };

    render() {
        return (
            <div className="container">
                <h3 className="title">Edit contact</h3>
                <Form contacts={this.state.contacts} back={this.back} changeName={this.changeName}
                      changePhone={this.changePhone} changeEmail={this.changeEmail} changePhoto={this.changePhoto}
                      add={this.saveContact}/>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        save: (contact, history) => dispatch(saveChanges(contact, history))
    };
};

export default connect(null, mapDispatchToProps)(Editor);