import React, {Component} from 'react'
import './WebApp.css';
import {connect} from "react-redux";
import {edit, getContacts, remove} from "../../store/actions";
import Modal from "../../components/Modal/Modal";
import Preloader from '../../assets/Preloader.gif';

class WebApp extends Component {
    state = {
        pickedContact: {},
        showModal: false
    };

    componentDidMount() {
        this.props.getContacts();
    }

    goToEditor = (contact) => {
        this.props.editContact(contact, this.props.history);
    };

    goToAdd = () => {
        this.props.history.push({pathname: '/add'})
    };

    showModal = (contact) => {
        this.setState({pickedContact: contact, showModal: true});
    };

    hideModal = (id) => {
        this.props.remove(id);
        this.setState({showModal: false});
    };

    render () {
        return (
            <div className="container">
                <img className="preloader" style={{display: this.props.loading ? 'block' : 'none'}} src={Preloader} alt=""/>
                {this.state.showModal ? <Modal toEdit={this.goToEditor} contact={this.state.pickedContact} remove={this.hideModal} /> : null}
                <div className="header">
                    <span>Contacts</span>
                    <button onClick={this.goToAdd}>Add new Contact</button>
                </div>
                <div className="body">
                    {this.props.contacts ? this.props.contacts.map(contact => {
                        return (
                            <div className="contacts-container" key={contact.id} onClick={() => this.showModal(contact)}>
                                <img src={contact.photo} alt=""/>
                                <h3>{contact.name}</h3>
                            </div>
                        )
                    }) : null}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getContacts: () => dispatch(getContacts()),
        remove: (id) => dispatch(remove(id)),
        editContact: (contact, history) => dispatch(edit(contact, history))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WebApp);