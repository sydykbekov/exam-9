import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import {connect} from "react-redux";
import {add} from "../../store/actions";

class AddNewContact extends Component {
    state = {
        contacts: {
            name: '',
            phone: '',
            email: '',
            photo: ''
        }
    };

    back = () => {
        this.props.history.push({
            pathname: '/'
        })
    };

    changeName = (event) => {
        const contacts = {...this.state.contacts};
        contacts.name = event.target.value;
        this.setState({contacts});
    };

    changePhone = (event) => {
        const contacts = {...this.state.contacts};
        contacts.phone = event.target.value;
        this.setState({contacts});
    };

    changeEmail = (event) => {
        const contacts = {...this.state.contacts};
        contacts.email = event.target.value;
        this.setState({contacts});
    };

    changePhoto = (event) => {
        const contacts = {...this.state.contacts};
        contacts.photo = event.target.value;
        this.setState({contacts});
    };

    addContact = () => {
        this.props.add(this.state.contacts, this.props.history);
    };

    render() {
        return (
            <div className="container">
                <h3 className="title">Add new contact</h3>
                <Form add={this.addContact} back={this.back} changeName={this.changeName} changePhone={this.changePhone}
                      changeEmail={this.changeEmail} changePhoto={this.changePhoto} contacts={this.state.contacts}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        add: (contacts, history) => dispatch(add(contacts, history))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewContact);