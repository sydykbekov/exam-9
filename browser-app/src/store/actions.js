import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';

export const startRequest = () => {
    return {type: START_REQUEST};
};

export const successRequest = (contacts) => {
    return {type: SUCCESS_REQUEST, value: contacts};
};

export const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const add = (contact, history) => {
    return dispatch => {
        dispatch(startRequest());
        axios.post('contacts.json', contact).then(() => {
            dispatch(successRequest());
            history.push({pathname: '/'});
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const getContacts = () => {
    return dispatch => {
        dispatch(startRequest());
        axios.get('contacts.json').then(response => {
            const contacts = [];
            for (let key in response.data) {
                contacts.unshift({...response.data[key], id: key});
            }
            dispatch(successRequest(contacts));
        }, error => {
            dispatch(errorRequest());
        })
    };
};

export const remove = (id) => {
    return dispatch => {
        dispatch(startRequest());
        axios.delete(`contacts/${id}.json`).then(() => {
            dispatch(getContacts());
        })
    };
};

export const edit = (contact, history) => {
    localStorage.setItem('contacts', JSON.stringify(contact));
    return dispatch => {
        history.push({pathname: '/edit'});
    };
};

export const saveChanges = (contact, history) => {
    return dispatch => {
        dispatch(startRequest());
        axios.put(`/contacts/${contact.id}.json`, contact).then(() => {
            history.push({pathname: '/'});
            dispatch(getContacts());
        });
    };
};
